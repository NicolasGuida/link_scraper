import logging
import os
import sys
import traceback
from datetime import datetime
from urllib.parse import urlparse

import pandas as pd
from selenium import webdriver as wb
from selenium.webdriver.chrome.options import Options

try:
    os.mkdir("./logs")
except Exception:
    pass

# creazione logger
logger = logging.getLogger("main")
logger.setLevel(logging.DEBUG)
# creazione degli handler per file e console
fh = logging.FileHandler("./logs/" + datetime.today().strftime('%Y-%m-%d_%H-%M-%S') + ".log", 'w', 'utf-8')
fh.setLevel(logging.WARNING)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.WARNING)

formatter = logging.Formatter(fmt='%(asctime)s.%(msecs)03d %(module)s %(lineno)d %(levelname)s: %(message)s',
                              datefmt='%H:%M:%S')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# aggiungi gli handler al logger
logger.addHandler(ch)
logger.addHandler(fh)
logger.propagate = False


def get_driver():
    options = Options()
    options.add_argument("--headless")
    options.add_argument(
        "user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36")
    PATH = "C:\Program Files (x86)\chromedriver.exe"
    driver = wb.Chrome(PATH, options=options)
    driver.set_page_load_timeout(20)
    return driver


def link_scrape(url):
    try:
        driver.get(url)
    except Exception:
        logger.warning("Errore da selenium")
        sys.exit(1)
    lista_link = []
    domain = urlparse(url).netloc
    elements = driver.find_elements_by_xpath("//a[@href]")
    for elem in elements:
        if urlparse(elem.get_attribute("href")).netloc == domain:
            product_link = elem.get_attribute("href")
            lista_link.append(product_link)
    df = pd.DataFrame(lista_link, columns=['Link'])
    df = df.drop_duplicates(subset=None)
    filename = urlparse(url).netloc + '_' + 'link_estratti' + '.csv'
    df.to_csv(filename, index=False)


if (len(sys.argv)) > 1:
    with open(sys.argv[1], 'r') as f:
        url = f.read()
else:
    with open('C:/Users/xxjok/Desktop/link_Scraper/link.txt', 'r') as f:
        url = f.read()

print("Il link fornito è: " + url)

try:
    driver = get_driver()
except Exception:
    logger.warning("Errore da get_driver")
    logger.debug(traceback.format_exc())
    sys.exit(1)

link_scrape(url)
driver.quit()
